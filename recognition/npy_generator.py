import sys
import os
import glob
import random

import numpy as np
import keras

def npy_generator(npy_path, batch_size, image_height, image_width, classes):
    npy_wildcard = npy_path + os.sep + '**' + os.sep + '*.npy'
    npy_path_list = glob.glob(npy_wildcard, recursive=True)

    random.shuffle(npy_path_list)
    processed = 0

    def find_pattern(string):
        for i in range(len(classes)):
            if classes[i] in string:
                return i
        raise ValueError('Unknown mesh category {}'.format(string))

    processed_batches = 0
    n = len(npy_path_list)
    while True:
        data = np.empty((batch_size, image_height, image_width, 1))
        labels = []
        for i in range(batch_size):
            idx = (i + processed_batches * batch_size) % n

            cur_path = npy_path_list[idx]
            
            data[i, :, :, 0] = np.load(cur_path)
            labels.append(find_pattern(cur_path))

        processed_batches += 1

        labels = keras.utils.to_categorical(labels, num_classes=len(classes))
        yield data, labels
